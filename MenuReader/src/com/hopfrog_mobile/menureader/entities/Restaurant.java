package com.hopfrog_mobile.menureader.entities;

import java.io.Serializable;

public class Restaurant implements Serializable {

	private static final long serialVersionUID = 2799603916591465947L;
	private String id;
	private String name;
	private String vicinity;
	private String description;
	private Menu menu;
	
	public Restaurant() {
		
	}
	
	public String getId() {
		return id;
	}
	
	public Menu getMenu() {
		return menu;
	}
	
	public String getName() {
		return name;
	}
	
	public String getVicinity() {
		return vicinity;
	}
	
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return String.format("Name: %s\nAddress: %s", name, vicinity);
	}
	
}
