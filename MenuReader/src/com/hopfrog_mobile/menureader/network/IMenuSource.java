package com.hopfrog_mobile.menureader.network;

import com.hopfrog_mobile.menureader.entities.RestaurantSearchResult;

public interface IMenuSource {

	public RestaurantSearchResult[] getNearbyRestaurants(double latitude, double longitude);
	
}
