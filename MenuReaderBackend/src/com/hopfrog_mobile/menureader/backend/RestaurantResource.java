package com.hopfrog_mobile.menureader.backend;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/restaurants")
public class RestaurantResource {
	
	private static Logger log = LoggerFactory.getLogger(RestaurantResource.class);
	
	@Context IDatabase db;
	
	@GET
	@Produces("application/json")
	public String getALLRestaurants() {
		log.info("Got a request for all restaurants.");
		return JSONUtils.list2JsonArrayObject("data", db.getAllRestaurants());
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public String getRestaurantById(@PathParam("id") String id) {
		log.info("Got a request for restaurant: {}", id);
		return JSONUtils.list2JsonArrayObject("data", db.getRestaurantById(id));
	}
	
	@POST
	@Path("/ids")
	@Produces("application/json")
	public String getRestaurantByIds(@FormParam("json") String json) {
		log.info("Got a request to get restaurants: {}", json);
		return JSONUtils.list2JsonArrayObject("data", db.getRestaurantByIds(json));
	}
	
	@POST
	@Produces("application/json")
	public void addRestaurant(@FormParam("json") String json) {
		log.info("Got a request to add restaurant: {}", json);
		db.addRestaurant(json);
	}
	
	@PUT
	@Path("/{id}")
	@Produces("application/json")
	public void updateRestaurant(@PathParam("id") String id, @FormParam("json") String json) {
		log.info("Got a request to update restaurant: {} with the entry: ", id, json);
	}
	
	@DELETE
	@Path("/{id}")
	@Produces("application/json")
	public void deleteRestaurant(@PathParam("id") String id) {
		log.info("Got a request to delete restaurant: {}", id);
	}
}
