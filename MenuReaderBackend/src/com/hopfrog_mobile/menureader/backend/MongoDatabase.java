package com.hopfrog_mobile.menureader.backend;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class MongoDatabase implements IDatabase{
	public static final String DEFAULT_ADDR = "127.0.0.1";
	public static final int DEFAULT_PORT = 27017;
	
	private static Logger log = LoggerFactory.getLogger(MongoDatabase.class);
	
	private MongoClient mongo;
	private DB db;
	private DBCollection dbc;
	
	public MongoDatabase(String address, int port) throws UnknownHostException {
		log.info("Starting mongo database at {}:{}", address, port);
		mongo = new MongoClient(address, port);
		db = mongo.getDB("menuReader");
		dbc = db.getCollection("restaurants");
	}

	@Override
	public List<String> getAllRestaurants() {
		ArrayList<String> results = new ArrayList<String>();
		BasicDBObject query = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject("_id",false);
		DBCursor cursor = dbc.find(query, fields);
		
		try {
			while(cursor.hasNext()) {
				results.add(cursor.next().toString());
			}
		} finally {
			cursor.close();
		}
		
		return results;
	}
	
	@Override
	public List<String> getRestaurantById(String id) {
		ArrayList<String> results = new ArrayList<String>();
		BasicDBObject query = new BasicDBObject("id", id);
		BasicDBObject fields = new BasicDBObject("_id", false);
		DBCursor cursor = dbc.find(query, fields);
		
		try {
			while(cursor.hasNext()) {
				results.add(cursor.next().toString());
			}
		} finally {
			cursor.close();
		}
		
		return results;
	}
	
	public List<String> getRestaurantByIds(String json) {
		ArrayList<String> results = new ArrayList<String>();
		BasicDBList idArray = (BasicDBList)((BasicDBObject)JSON.parse(json)).get("ids");
		BasicDBObject query = new BasicDBObject("id", new BasicDBObject("$in", idArray));
		BasicDBObject fields = new BasicDBObject("_id", false);
		
		DBCursor cursor = dbc.find(query, fields);
		
		try {
			while(cursor.hasNext()) {
				results.add(cursor.next().toString());
			}
		} finally {
			cursor.close();
		}
		
		return results;
	}

	@Override
	public void addRestaurant(String json) {
		BasicDBObject obj = (BasicDBObject)JSON.parse(json);
		dbc.insert(obj);
	}
}
