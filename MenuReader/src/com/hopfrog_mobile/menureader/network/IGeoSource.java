package com.hopfrog_mobile.menureader.network;

import android.util.Pair;

public interface IGeoSource {
	public Pair<Float, Float> getLatLong(String address);
}
