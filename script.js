db.restaraunts.insert(
	{
	id: "",
	name: "Moes",
	location: "your_house",
	menu : {
		items: [
			{
				name: "Pad Thai",
				veg: "y",
				catagory: "noodles",
				description: "The cherished dish of Thailand. Thin rice noodles in a Thai citrus sauce w/ egg, sautéed chicken, bean sprouts, green onions, carrots and a dash of crushed peanuts",
				cost: "7"	
			},
			{
				name: "Pad Woon Sen",
				veg: "y",
				catagory: "noodles",
				description: "A little known gem, this light & pleasing vermicelli rice noodle dish is stir-fried in light brown sauce w/ egg, sauteed chicken, carrots, white onions, green onions, cabbage, mushroom, and bean sprout.",
				cost: "7"	
			},
			{
				name: "Sing Chow Men",
				veg: "y",
				catagory: "noodles",
				description: "Egg, sautéed chicken, carrots, white & green onions, cabbage, mushrooms and jalapenos compliment the chow fun noodles & warm yellow curry seasoning of this exciting dish.",
				cost: "7"	
			},
			{
				name: "Lo Men",
				veg: "y",
				catagory: "noodles",
				description: "Yaki soba noodles, stir-fried in a Thai-brown sauce with sautéed chicken, carrots, white onions, green onions, cabbage and mushrooms.",
				cost: "7"	
			},
			{
				name: "Spicy Spaghetti",
				veg: "n",
				catagory: "noodles",
				description: "A fiery fusion of sautéed chicken, basil, white onions, green onions, tomatoes and jalapenos in a mild chili sauce.",
				cost: "7"	
			},
			{
				name: "Cantonese Noodle",
				veg: "y",
				catagory: "noodles",
				description: "A simple & comforting dish from Hong Kong. Yaki soba noodles tossed with bean sprouts, green onions, baby spinach and grilled steak in a brown sauce.",
				cost: "7"	
			},
			{
				name: "Tom Yum Noodle Soup",
				veg: "n",
				catagory: "noodles",
				description: "The famous Thai lemon grass and chili soup with shrimp, white onions, mushrooms, cilantro and the always pleasing thin rice noodles.",
				cost: "7"	
			},
			{
				name: "Vietnamese Beef Pho",
				veg: "n",
				catagory: "noodles",
				description: "Our version of this classic dish. Piping hot broth and rice noodles flavored with Asian herbs with fresh basil, bean sprouts, jalapenos and hoisin sauce.",
				cost: "7"	
			},
			{
				name: "Hamburger",
				description: "Beef, with cheese and tomato",
				cost: "5"	
			}
		]
	}
});
