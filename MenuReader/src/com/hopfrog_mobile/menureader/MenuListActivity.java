package com.hopfrog_mobile.menureader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hopfrog_mobile.menureader.entities.MenuItem;
import com.hopfrog_mobile.menureader.entities.Restaurant;

public class MenuListActivity extends Activity {
	
	public static final int VEG_SHOW = 0;
	public static final int VEG_HIDE = 1;
	public static final int VEG_ONLY = 2;
	
	private MenuItemAdapter adapter;
	private ListView listView;
	private Dialog categoryDialog;
	private SharedPreferences settings;
	
	private HashMap<String, HashSet<MenuItem>> categories = new HashMap<String, HashSet<MenuItem>>();
	private HashMap<String, Boolean> showing = new HashMap<String, Boolean>();
	private HashSet<MenuItem> vegetarian_items = new HashSet<MenuItem>();
	private int show_vegetarian = VEG_SHOW;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_menu_list);
		
		settings = this.getSharedPreferences("MenuReaderSettings", 0);
		
		List<MenuItem> items = new ArrayList<MenuItem>();
		items.addAll(((Restaurant)this.getIntent().getSerializableExtra("restaurant")).getMenu().getItems());
		
		for(MenuItem i : items) {
			String category = i.getCategory();
			if(!categories.containsKey(category)) {
				categories.put(category, new HashSet<MenuItem>());
			}
			if(i.isVeg()) {
				vegetarian_items.add(i);
			}
			categories.get(category).add(i);
		}
		
		for(String category : categories.keySet()) {
			showing.put(category, true);
		}
		
		adapter = new MenuItemAdapter(this, android.R.layout.simple_list_item_1, items);
		
		listView = (ListView) this.findViewById(R.id.menu_list_listview);
		listView.setAdapter(adapter);
		
		//Set menu description 
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
				TextView itemName = (TextView) view.findViewById(R.id.menu_item_list_name);
				TextView description = (TextView) view.findViewById(R.id.menu_item_list_description);
				if (itemName.getTransformationMethod() != null) {
					if(!description.getText().toString().trim().equals(""))
						description.setVisibility(TextView.VISIBLE);
					itemName.setSingleLine(false);
				} else {
					description.setVisibility(TextView.GONE);
					itemName.setSingleLine(true);
				}
			}
		});
	}
	
	public void onResume() {
		super.onResume();
		adapter.updateFontMultiplier();
		adapter.notifyDataSetChanged();
	}
	
	public void onPause() {
		super.onPause();
		if(categoryDialog != null && categoryDialog.isShowing()) {
			categoryDialog.dismiss();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_list, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == R.id.action_filter) {
			categoryDialog = createDialog();
			categoryDialog.show();
		} 
		else if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return true;
	}
	
	private void updateMenuList() {
		HashSet<MenuItem> items = new HashSet<MenuItem>();
		adapter.clear();
		
		for(Entry<String, Boolean> entry : showing.entrySet()) {
			if(entry.getValue()) {
				items.addAll(categories.get(entry.getKey()));
			}
		}
		
		if(show_vegetarian == VEG_HIDE) {
			items.removeAll(vegetarian_items);
		} else if(show_vegetarian == VEG_ONLY) {
			items.retainAll(vegetarian_items);
		}
		
		adapter.addAll(items);
		adapter.notifyDataSetChanged();
	}
	
	private static final int SECTION_SIZE = 18;
	private static final int FONT_SIZE = 14;
	
	private Dialog createDialog() {
		float fontMultiplier = settings.getFloat("fontMultiplier", 1F);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		ScrollView sview = (ScrollView) inflater.inflate(R.layout.dialog_filter, null);
		LinearLayout view = (LinearLayout) sview.findViewById(R.id.dialog_cotent);
		
		TextView vegOpts = (TextView) view.findViewById(R.id.text_veg_opts);
		vegOpts.setTextSize(SECTION_SIZE * fontMultiplier);
		TextView catText = (TextView) view.findViewById(R.id.text_categories);
		catText.setTextSize(SECTION_SIZE * fontMultiplier);
		
		final RadioButton rb1 = (RadioButton) view.findViewById(R.id.vegetarian_show);
		rb1.setTextSize(FONT_SIZE * fontMultiplier);
		final RadioButton rb2 = (RadioButton) view.findViewById(R.id.vegetarian_hide);
		rb2.setTextSize(FONT_SIZE * fontMultiplier);
		final RadioButton rb3 = (RadioButton) view.findViewById(R.id.vegetarian_show_only);
		rb3.setTextSize(FONT_SIZE * fontMultiplier);
		
		switch(show_vegetarian) {
		case VEG_SHOW:
			rb1.setChecked(true);
			break;
		case VEG_HIDE:
			rb2.setChecked(true);
			break;
		case VEG_ONLY:
			rb3.setChecked(true);
			break;
		}
		
		final ArrayList<CheckBox> cbs = new ArrayList<CheckBox>();
		for(Entry<String, Boolean> entry : showing.entrySet()) {
			CheckBox cb = new CheckBox(this);
			cb.setTextSize(FONT_SIZE * fontMultiplier);
			cbs.add(cb);
			cb.setText(entry.getKey());
			cb.setChecked(entry.getValue());
			view.addView(cb);
		}
		
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if(rb1.isChecked()) {
                	show_vegetarian = VEG_SHOW;
                } else if (rb2.isChecked()) {
                	show_vegetarian = VEG_HIDE;
                } else if (rb3.isChecked()) {
                	show_vegetarian = VEG_ONLY;
                }
                
                for(CheckBox cb : cbs) {
                	showing.put(cb.getText().toString(), cb.isChecked());
        		}
                
                updateMenuList();
            }
        });
		
		builder.setView(sview);
		builder.setTitle(R.string.choose_categories);
		
		return builder.create();
	}
}
