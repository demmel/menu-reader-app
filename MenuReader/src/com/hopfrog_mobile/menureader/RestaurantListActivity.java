package com.hopfrog_mobile.menureader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hopfrog_mobile.menureader.entities.Restaurant;
import com.hopfrog_mobile.menureader.entities.RestaurantSearchResult;
import com.hopfrog_mobile.menureader.network.MenuSource;
import com.hopfrog_mobile.menureader.network.RestfulDatabaseImpl;

public class RestaurantListActivity extends Activity implements LocationListener {

	private LocationManager locationManager;
	private Location location;
	private List<Restaurant> searchResults;
	private RestaurantAdapter adapter;
	
	private ProgressDialog dialog;
	float lat, lng;
	boolean hasLatLng = false;
	Restaurant[] restaurantsSaved = null;
	
	/* Start lifecycle code */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_list);
		
		Bundle extras = getIntent().getExtras();
		if ( extras != null && extras.containsKey("lat") && extras.containsKey("lng") ) {
			hasLatLng = true;
			lat = extras.getFloat("lat");
			lng = extras.getFloat("lng");
		}
		
		searchResults = new ArrayList<Restaurant>();
		
		adapter = new RestaurantAdapter(this, android.R.layout.simple_list_item_1, searchResults);
		ListView listView = (ListView) findViewById(R.id.restaurant_listview);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
				Intent i = new Intent(RestaurantListActivity.this, MenuListActivity.class);
				i.putExtra("restaurant", adapter.getItem(index));
				startActivity(i);
			}
		});
		registerForContextMenu(listView);
		
		if(savedInstanceState != null && savedInstanceState.containsKey("restaurants")) {
			restaurantsSaved = (Restaurant[]) savedInstanceState.getSerializable("restaurants");
			for (Restaurant r : restaurantsSaved) {
				adapter.add(r);
			}
		} else {
			locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
			//Loading dialog
			dialog = ProgressDialog.show(this,
					Html.fromHtml("<h1>Finding restaurants near you.</h1>"),
					Html.fromHtml("<h1>Please wait while we pinpoint your location.</h1>"),
					false); 
			
	        if (!hasLatLng) {
	        	turnOnGPS();
	        } else {
	        	new RestaurantListTask().execute();
	        }
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		if(restaurantsSaved != null)
			savedInstanceState.putSerializable("restaurants", restaurantsSaved);
	}
	
	@Override
	public void onResume() {
	    super.onResume();
		adapter.updateFontMultiplier();
		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}	
	}
	/* End lifecycle code */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
	  if (v.getId()==R.id.restaurant_listview) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
	    menu.setHeaderTitle(adapter.getItem(info.position).getName());
	    menu.add(Menu.NONE, 0, 0, "Add to favorites");
	  }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
		int menuItemIndex = item.getItemId();
		if (menuItemIndex == 0) {
			SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);
			Restaurant save = adapter.getItem(info.position);
			String favorites = settings.getString("favorites", "");
			if (!favorites.contains(save.getId())) {
				favorites += "," + save.getId();
		        SharedPreferences.Editor editor = settings.edit();
		        editor.putString("favorites", favorites);
		        editor.commit();
		        System.out.println(favorites);
			}
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return true;
	}	

	
	/* Start Location Code */
	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		locationManager.removeUpdates(RestaurantListActivity.this);
		new RestaurantListTask().execute();
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
	}
	
	private void turnOnGPS() {
		try {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, RestaurantListActivity.this);
		} catch (Exception e) {
			Log.wtf("RestaurantList", e.getMessage());
		}
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, RestaurantListActivity.this);
	}
	/* End Location Code */
	
	
	/*
	 * Tech Square GPS
	 * lng : -84.38971
	 * lat: 33.77671
	 */
	private class RestaurantListTask extends AsyncTask<Void, Void, Restaurant[]> {

	    protected void onPreExecute() {
	        
	    }
		
		protected Restaurant[] doInBackground(Void... voids) {
			if (hasLatLng) {
				//Do the Google Places call
				MenuSource menuSource = new MenuSource(500);
				RestaurantSearchResult[] results = menuSource.getNearbyRestaurants(lat, lng);
				
				RestfulDatabaseImpl db = new RestfulDatabaseImpl();
				try {
					Restaurant[] restaurants = db.getRestaurantsFromResults(results);
					return restaurants;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (location != null) {
				//Do the Google Places call
				MenuSource menuSource = new MenuSource(500);
				double lat = location.getLatitude();
				double lng = location.getLongitude();
				RestaurantSearchResult[] results = menuSource.getNearbyRestaurants(lat, lng);
				
				RestfulDatabaseImpl db = new RestfulDatabaseImpl();
				try {
					Restaurant[] restaurants = db.getRestaurantsFromResults(results);
					return restaurants;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	    	
	    	return null;
	     }

		protected void onProgressUpdate(Void... voids) {
		}

		protected void onPostExecute(Restaurant[] restaurants) {
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

			restaurantsSaved = restaurants;
			if (restaurants != null) {
				System.out.println(Arrays.toString(restaurants));
				for (Restaurant r : restaurants) {
					adapter.add(r);
				}
			}
		}
	}
}
