package com.hopfrog_mobile.menureader.backend;

import java.util.List;

public interface IDatabase {

	public List<String> getAllRestaurants();
	public List<String> getRestaurantById(String id);
	public List<String> getRestaurantByIds(String json);
	public void addRestaurant(String json);
	
}
