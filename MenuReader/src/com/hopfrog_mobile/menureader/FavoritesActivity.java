package com.hopfrog_mobile.menureader;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hopfrog_mobile.menureader.entities.Restaurant;
import com.hopfrog_mobile.menureader.network.RestfulDatabaseImpl;

public class FavoritesActivity extends Activity {

	private List<Restaurant> favorites;
	private RestaurantAdapter adapter;
	
	private ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_list);
				
		favorites = new ArrayList<Restaurant>();
		
		adapter = new RestaurantAdapter(this, android.R.layout.simple_list_item_1, favorites);
		ListView listView = (ListView) findViewById(R.id.restaurant_listview);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
				Intent i = new Intent(FavoritesActivity.this, MenuListActivity.class);
				i.putExtra("restaurant", adapter.getItem(index));
				startActivity(i);
			}
		});
	
		//Loading dialog
		dialog = ProgressDialog.show(this,
				Html.fromHtml("<h1>Finding your favorites.</h1>"),
				Html.fromHtml("<h1>Please wait while we load your menus.</h1>"),
				false); 
		
		new RestaurantListTask().execute();
		
		registerForContextMenu(listView);
		
		
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
	  if (v.getId()==R.id.restaurant_listview) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
	    menu.setHeaderTitle(adapter.getItem(info.position).getName());
	    menu.add(Menu.NONE, 0, 0, "Remove from favorites");
	  }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
		int menuItemIndex = item.getItemId();
		if (menuItemIndex == 0) {
			SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);
			Restaurant save = adapter.getItem(info.position);
			String favorites = settings.getString("favorites", "");
			if (favorites.contains(save.getId())) {
				favorites = favorites.replaceAll("(,)?"+save.getId(), "");
		        SharedPreferences.Editor editor = settings.edit();
		        editor.putString("favorites", favorites);
		        editor.commit();
		        adapter.remove(save);
		        adapter.notifyDataSetChanged();
		        System.out.println(favorites);
			}
		}
		return true;
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return true;
	}	
	
	@Override
	protected void onResume() {
	    super.onResume();
		adapter.updateFontMultiplier();
		adapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		
	}


	private class RestaurantListTask extends AsyncTask<Void, Void, Restaurant[]> {
		
		protected Restaurant[] doInBackground(Void... voids) {
	    	RestfulDatabaseImpl db = new RestfulDatabaseImpl();
	    	SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);
			String favorites = settings.getString("favorites", "");
			try {
				return db.getRestaurantsFromIds(favorites.split(","));
			} catch (Exception e) {
				System.err.println("No internet connection");
			}
	    	return null;
	     }

		protected void onProgressUpdate(Void... voids) {
		}

		protected void onPostExecute(Restaurant[] restaurants) {
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

			if (restaurants != null) {
				for (Restaurant r : restaurants) {
					adapter.add(r);
				}
			}
		}
	}
}
