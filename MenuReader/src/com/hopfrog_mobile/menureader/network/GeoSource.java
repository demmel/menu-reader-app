package com.hopfrog_mobile.menureader.network;

import java.net.URI;
import java.net.URLEncoder;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import android.annotation.SuppressLint;
import android.util.Pair;

public class GeoSource implements IGeoSource {
	
	String url = "http://www.datasciencetoolkit.org/maps/api/geocode/json";

	@Override
	public Pair<Float, Float> getLatLong(String address) {
		try {
			String fullURL = buildURL(address);
			String json = IOUtils.toString(new URI(fullURL));
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(json).getAsJsonObject();
			JsonArray results = obj.getAsJsonArray("results");
			JsonObject loc = results.get(0).getAsJsonObject().getAsJsonObject("geometry").getAsJsonObject("location");
			float lat = loc.get("lat").getAsFloat();
			float lng = loc.get("lng").getAsFloat();
			return new Pair<Float, Float>(lat, lng);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Private functions
	@SuppressLint("DefaultLocale")
	private String buildURL(String address) {
		try {
			return String.format("%s?%s&address=%s", url, "sensor=false", URLEncoder.encode(address, "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
