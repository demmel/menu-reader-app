package com.hopfrog_mobile.menureader.entities;

import java.io.Serializable;

public class MenuItem implements Serializable {

	private static final long serialVersionUID = 8500930050621755582L;
	private String name;
	private String description;
	private String category;
	private boolean veg;
	private double cost;
	
	public MenuItem() {
		
	}

	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}

	public String getCategory() {
		return category;
	}

	public boolean isVeg() {
		return veg;
	}

	public double getCost() {
		return cost;
	}
	
	@Override
	public String toString() {
		return "MenuItem [name=" + name + ", description=" + description
				+ ", category=" + category + ", veg=" + veg + ", cost=" + cost
				+ "]";
	}
	
}
