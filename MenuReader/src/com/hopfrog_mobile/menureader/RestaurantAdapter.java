package com.hopfrog_mobile.menureader;

import java.util.List;

import com.hopfrog_mobile.menureader.entities.Restaurant;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RestaurantAdapter extends ArrayAdapter<Restaurant> {
	
	public static final int NAME_SIZE = 18;
	public static final int ADDR_SIZE = 14;
	
	private static LayoutInflater inflater;
	private static SharedPreferences settings;
	private float fontMultiplier;

	public RestaurantAdapter(Context context, int textViewResourceId,
			List<Restaurant> objects) {
		super(context, textViewResourceId, objects);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		settings = context.getSharedPreferences("MenuReaderSettings", 0);
		updateFontMultiplier();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.restaurant_list_item, null);
		}
		
		Restaurant r = this.getItem(position);
		
	    TextView name = (TextView) convertView.findViewById(R.id.restaurant_list_item_name);
	    TextView address = (TextView) convertView.findViewById(R.id.restaurant_list_item_address);
	    
	    name.setText(r.getName());
	    name.setTextSize(NAME_SIZE * fontMultiplier);
	    address.setText(r.getVicinity());
	    address.setTextSize(ADDR_SIZE * fontMultiplier);
	
	    return convertView;
	  }
	
	public void updateFontMultiplier() {
		fontMultiplier = settings.getFloat("fontMultiplier", 1F);
	}

}
