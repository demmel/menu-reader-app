package com.hopfrog_mobile.menureader.entities;

public class RestaurantSearchResult {

	private String id;
	private String name;
	private String vicinity;
	//private double lat, lng;
	
	public RestaurantSearchResult() {
		
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getVicinity() {
		return vicinity;
	}

	@Override
	public String toString() {
		return "RestaurantSearchResult [id=" + id + ", name=" + name
				+ ", vicinity=" + vicinity + "]";
	}

}
