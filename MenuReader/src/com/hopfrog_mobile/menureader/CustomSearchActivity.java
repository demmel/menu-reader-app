package com.hopfrog_mobile.menureader;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.hopfrog_mobile.menureader.network.GeoSource;

public class CustomSearchActivity extends Activity {
	
	private final int BUTTON_SIZE = 24;
	private final int TEXT_SIZE = 24;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom_search);
		
		Button button = (Button) findViewById(R.id.custom_search_search_button);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new RestaurantListTask().execute();
			}
		});
		

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		updateTextSize();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		updateTextSize();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return true;
	}	
	
	private void updateTextSize() {
		SharedPreferences prefs = this.getSharedPreferences("MenuReaderSettings", 0);
		float fontMultiplier = prefs.getFloat("fontMultiplier", 1F);
		Button button = (Button) findViewById(R.id.custom_search_search_button);
		EditText text = (EditText) findViewById(R.id.custom_search_address_edittext);
		button.setTextSize(BUTTON_SIZE * fontMultiplier);
		text.setTextSize(TEXT_SIZE * fontMultiplier);
	}
	
	private class RestaurantListTask extends AsyncTask<Void, Void, Pair<Float,Float>> {
		
		protected Pair<Float, Float> doInBackground(Void... voids) {
			String address = ((EditText) CustomSearchActivity.this.findViewById(R.id.custom_search_address_edittext)).getText().toString();
			GeoSource geo = new GeoSource();
			return geo.getLatLong(address);
	     }

		protected void onProgressUpdate(Void... voids) {
		}

		protected void onPostExecute(Pair<Float,Float> latlng) {
			if (latlng == null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(CustomSearchActivity.this);
				builder.setTitle("Error");
				builder.setMessage("Could not find a location for your address.");
				builder.create().show();
			} else {
				Intent i = new Intent(CustomSearchActivity.this, RestaurantListActivity.class);
				i.putExtra("lat", latlng.first);
				i.putExtra("lng", latlng.second);
				CustomSearchActivity.this.startActivity(i);
			}
		}
	}
	
}
