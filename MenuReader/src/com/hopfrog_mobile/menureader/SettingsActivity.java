package com.hopfrog_mobile.menureader;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SettingsActivity extends Activity {
	final int DEFAULT_FONT_SIZE = 24;
	SeekBar sb;
	int maxValue = 250;
	float saveValue = 0;
	View restaurantSample;
	View menuItemSample;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_settings);
		LayoutInflater inflater = this.getLayoutInflater();
		
		LinearLayout ll = (LinearLayout) this.findViewById(R.id.SettingsLayout);
		restaurantSample = inflater.inflate(R.layout.restaurant_list_item, null);
		menuItemSample = inflater.inflate(R.layout.menu_item_list_item, null);
		
		ll.addView(restaurantSample, 5);
		ll.addView(menuItemSample, 8);
		
		sb = (SeekBar)findViewById(R.id.seekBar1);
		sb.setMax(maxValue);
		
		SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);		
		float multiplier = settings.getFloat("fontMultiplier", 1F);
		saveValue = multiplier;
		
		updateRestaurantSample();
		updateMenuItemSample();
		
		//Convert multiplier to INT for seekbar
		sb.setProgress((int) ((multiplier - 1) * 100) );
		
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged(SeekBar arg0, int newValue, boolean arg2) {
				//Convert into to float multiplier
				saveValue = 1 + (newValue/100.0F);
				updateRestaurantSample();
				updateMenuItemSample();
			}
	
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				
			}
	
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				
			}
			
		});
 	}
	
	private void updateRestaurantSample() {		
	    TextView name = (TextView) restaurantSample.findViewById(R.id.restaurant_list_item_name);
	    TextView address = (TextView) restaurantSample.findViewById(R.id.restaurant_list_item_address);
	    
	    name.setTextSize(RestaurantAdapter.NAME_SIZE * saveValue);
	    address.setTextSize(RestaurantAdapter.ADDR_SIZE * saveValue);
	}
	
	private void updateMenuItemSample() {
		TextView name = (TextView) menuItemSample.findViewById(R.id.menu_item_list_name);
		TextView cost = (TextView) menuItemSample.findViewById(R.id.menu_item_list_cost);
		TextView description = (TextView) menuItemSample.findViewById(R.id.menu_item_list_description);
		ImageView veg = (ImageView) menuItemSample.findViewById(R.id.menu_item_list_veg);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				(int)(MenuItemAdapter.COST_SIZE * saveValue),(int)(MenuItemAdapter.COST_SIZE * saveValue));
		veg.setLayoutParams(lp);
		
		veg.setVisibility(View.VISIBLE);
		name.setTextSize(MenuItemAdapter.NAME_SIZE * saveValue);
		name.setSingleLine(false);
		cost.setTextSize(MenuItemAdapter.COST_SIZE * saveValue);
		description.setTextSize(MenuItemAdapter.DESC_SIZE * saveValue);
		description.setVisibility(View.VISIBLE);
	}
	
	public void onBackPressed(){
		super.onBackPressed();
        SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat("fontMultiplier", saveValue);
        editor.commit();
	}
}
