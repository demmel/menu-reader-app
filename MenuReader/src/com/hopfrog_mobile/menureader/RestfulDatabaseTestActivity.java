package com.hopfrog_mobile.menureader;

import java.io.IOException;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.hopfrog_mobile.menureader.entities.Restaurant;
import com.hopfrog_mobile.menureader.network.RestfulDatabaseImpl;

public class RestfulDatabaseTestActivity extends Activity {

	TextView tx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		tx = new TextView(this);
		tx.setText("Querying Database");
		this.setContentView(tx);
		
		new DatabaseQueryTask().execute();
	}

	private class DatabaseQueryTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try {
				StringBuilder sb = new StringBuilder("");
				Restaurant[] restaurants = new RestfulDatabaseImpl().getAllRestaurants();
				for(Restaurant r : restaurants) {
					sb.append(r);
					sb.append("\n");
				}
				return sb.toString();
			} catch (IOException e) {
				return "A n error occured: " + e;
			}
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			tx.setText(result);
		}
		
	}
}
