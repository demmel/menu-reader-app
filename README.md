Menu Reader
===========

Overview
--------
The purpose of this app is make it easy for visually impaired people to get the
menus of restaurants in a form they can easily interpret on their smartphone or
tablet.
