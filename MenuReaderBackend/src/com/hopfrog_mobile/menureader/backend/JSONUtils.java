package com.hopfrog_mobile.menureader.backend;

import java.util.Iterator;
import java.util.List;

public class JSONUtils {
	public static String list2JsonArrayObject(String name, List<String> list) {
		StringBuilder sb = new StringBuilder("{\"" + name + "\":[");
		Iterator<String> it = list.iterator();
		
		while(it.hasNext()) {
			sb.append(it.next());
			if(it.hasNext())
				sb.append(',');
		}
		
		sb.append("]}");
		return sb.toString();
	}
}
