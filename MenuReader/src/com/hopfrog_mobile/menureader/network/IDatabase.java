package com.hopfrog_mobile.menureader.network;

import java.io.IOException;

import com.hopfrog_mobile.menureader.entities.Restaurant;
import com.hopfrog_mobile.menureader.entities.RestaurantSearchResult;

public interface IDatabase {
	public Restaurant[] getAllRestaurants() throws IOException;
	public Restaurant[] getRestaurantsFromResults(RestaurantSearchResult[] searchResults) throws IOException;
	public Restaurant[] getRestaurantsFromIds(String[] ids) throws IOException;
	public Restaurant getRestaurantByResult(RestaurantSearchResult searchResult) throws IOException;
	public Restaurant getRestaurantById(String id) throws IOException;
}
