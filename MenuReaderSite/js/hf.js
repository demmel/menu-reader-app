$(document).ready(function () {
    function MenuModel() {
        var self = this;

        self.name = ko.observable("");
        self.description = ko.observable("");
        self.id = ko.observable("");
        self.vicinity = ko.observable("");

        self.menuItems = ko.observableArray();

    	self.newName = ko.observable("");
    	self.newDescription = ko.observable("");
        self.newCategory = ko.observable("");
        self.newCost = ko.observable("");
        self.newVeg = ko.observable(false);

        self.errors = ko.observable("");

        self.success = ko.observable("");

        self.addMenuItem = function() {
            var newItem = {};
            newItem.name = self.newName();
            newItem.description = self.newDescription();
            newItem.cost = self.newCost();
            newItem.category = self.newCategory();
            newItem.veg = self.newVeg();
            self.menuItems.unshift(newItem);
            self.newName("");
            self.newDescription("");
            self.newCost("");
            self.newCategory("");
            self.newVeg(false);
        };

        self.deleteMenuItem = function(menuItem) {
            self.menuItems.remove(menuItem);
        };

        self.editMenuItem = function(menuItem) {
            self.newName(menuItem.name);
            self.newDescription(menuItem.description);
            self.newCategory(menuItem.category);
            self.newCost(menuItem.cost);
            self.newVeg(menuItem.veg);
            self.menuItems.remove(menuItem);
        };

        self.checkAddEnable = ko.computed( function() {
            if ( isNaN(self.newCost()) ) {
                self.errors("Please only enter numbers for cost.");
                return false;
            } else {
                self.errors("");
            }
            return self.newName().length > 0;
        });

        self.canSendToServer = ko.computed(function() {
            return self.menuItems().length > 0 && self.name().length > 0 &&
            self.id().length > 0 && self.vicinity().length > 0; 
        });

        self.sendMenuToServer = function() {
            rest = {};
            rest.name = self.name();
            rest.description = self.description();
            rest.id = self.id();
            rest.vicinity = self.vicinity();
            rest.menu = {};
			rest.menu.items = self.menuItems();
            json = JSON.stringify(rest);

			$.ajax({
				type: "POST",
				url: 'http://hopfrog.ignorelist.com:8080/restaurants',
				data: {json : json},
				success: function() {
					self.success("We have successfully received your menu.  Thanks!");
				},
				dataType: 'json'
			});
        };

    }

    ko.applyBindings(new MenuModel());
});