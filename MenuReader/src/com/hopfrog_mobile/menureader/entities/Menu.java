package com.hopfrog_mobile.menureader.entities;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class Menu implements Serializable {
	
	private static final long serialVersionUID = -5597660525584875389L;
	List<MenuItem> items;
	
	public Menu() {
		
	}
	
	public List<MenuItem> getItems() {
		return Collections.unmodifiableList(items);
	}

	@Override
	public String toString() {
		return "Menu [items=" + items + "]";
	}

}
