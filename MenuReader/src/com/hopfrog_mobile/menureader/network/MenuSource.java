package com.hopfrog_mobile.menureader.network;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.io.IOUtils;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hopfrog_mobile.menureader.entities.RestaurantSearchResult;

@SuppressLint("DefaultLocale")
public class MenuSource implements IMenuSource {

	private String placesURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
	private String apiKey = "AIzaSyC3gG9c8TjfgJPIMUflUK4qOjAUQyn38S0";
	private int radius;
	
	//Constructors
	public MenuSource(int radius) {
		setRadius(radius);
	}
	
	public MenuSource() {
		this(500);
	}

	//Implemented methods
	@Override
	public RestaurantSearchResult[] getNearbyRestaurants(double latitude, double longitude) {
		RestaurantSearchResult[] restaurants = null;
		
		try {
			String fullURL = buildURL(latitude, longitude);
			String json = IOUtils.toString(new URI(fullURL));
			
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(json).getAsJsonObject();
			String restaurantArray = obj.getAsJsonArray("results").toString();
			System.err.println(restaurantArray);
			restaurants = new Gson().fromJson(restaurantArray, RestaurantSearchResult[].class);
		} catch (IOException ioe) {
			System.err.println("Network error fetching restaurant list");
			ioe.printStackTrace();
		} catch (URISyntaxException urise) {
			System.err.println("Malformed restaurant list URI");
			urise.printStackTrace();
		}
		
		return restaurants;
	}
	
	
	//Etc functions
	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	
	//Private functions
	@SuppressLint("DefaultLocale")
	private String buildURL(double latitude, double longitude) {
		return String.format("%s?key=%s&rankby=distance&sensor=true&types=food&location=%f,%f",
				placesURL, apiKey, latitude, longitude);
	}

}
