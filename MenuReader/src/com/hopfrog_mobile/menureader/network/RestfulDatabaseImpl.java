package com.hopfrog_mobile.menureader.network;

import java.io.IOException;
import java.net.URLEncoder;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.hopfrog_mobile.menureader.entities.Restaurant;
import com.hopfrog_mobile.menureader.entities.RestaurantSearchResult;
import com.hopfrog_mobile.menureader.utils.HttpUtils;

public class RestfulDatabaseImpl implements IDatabase {
	private String TAG = "RestfulDatabaseImpl";
	
	private String address = "http://hopfrog.ignorelist.com:8080";

	@Override
	public Restaurant[] getAllRestaurants() throws IOException {
		String json = HttpUtils.sendGetRequest(address, "restaurants", "");
		Restaurant[] restaurants = json2Restaurants(json);
		return restaurants;
	}

	@Override
	public Restaurant[] getRestaurantsFromResults(RestaurantSearchResult[] searchResults) throws IOException {
		String[] ids = new String[searchResults.length];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = searchResults[i].getId();
		}
		return getRestaurantsFromIds(ids);
	}
	
	@Override
	public Restaurant[] getRestaurantsFromIds(String[] ids) throws IOException {
		JsonArray arr = new JsonArray();
		for(String id : ids) {
			arr.add(new JsonPrimitive(id));
		}
		
		JsonObject obj = new JsonObject();
		obj.add("ids", arr);
		
		String json = HttpUtils.sendPostRequest(address, "restaurants/ids", "json=" + URLEncoder.encode(obj.toString(), "UTF-8"));
		
		return json2Restaurants(json);
	}
	
	@Override
	public Restaurant getRestaurantByResult(RestaurantSearchResult searchResult) throws IOException {
		return getRestaurantById(searchResult.getId());
	}
	
	@Override
	public Restaurant getRestaurantById(String id) throws IOException {
		String json = HttpUtils.sendGetRequest(address, "restaurants/" + id, "");
		Log.d(TAG, json);
		
		Restaurant[] restaurants = json2Restaurants(json);
		if(restaurants.length > 0)
			return restaurants[0];
		else
			return null;
	}
	
	private Restaurant[] json2Restaurants(String json) {
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		String array = obj.getAsJsonArray("data").toString();
		return new Gson().fromJson(array, Restaurant[].class);
	}
}
