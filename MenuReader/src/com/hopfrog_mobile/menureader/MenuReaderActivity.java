package com.hopfrog_mobile.menureader;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuReaderActivity extends Activity {

	private final int FONT_SIZE = 24;
	private Button nearbyButton, customButton, favoritesButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_reader);
		
		fontSizeFix();
		
		//Find nearby
		nearbyButton = (Button) findViewById(R.id.find_nearby_button);
		nearbyButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(v.getContext(), RestaurantListActivity.class));
			}
		});
		
		//Custom Search
		customButton = (Button) findViewById(R.id.custom_search_button);
		customButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(v.getContext(), CustomSearchActivity.class));
			}
		});	
		
		//Favorites
		favoritesButton = (Button) findViewById(R.id.favorites_button);
		favoritesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(v.getContext(), FavoritesActivity.class));
			}
		});	
		
		updateFontSizes();
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		updateFontSizes();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return true;
	}
	
	
	private void updateFontSizes() {
		float fontMultiplier = getSharedPreferences("MenuReaderSettings", 0).getFloat("fontMultiplier", 1F);
		nearbyButton.setTextSize(FONT_SIZE * fontMultiplier);
		customButton.setTextSize(FONT_SIZE * fontMultiplier);
		favoritesButton.setTextSize(FONT_SIZE * fontMultiplier);
	}
	
	
	/**
	 * Hacky fix to make sure that a float is really stored in the multiplier
	 * variable.  Otherwise we get a casting exception.
	 */
	private float fontSizeFix() {
		SharedPreferences settings = getSharedPreferences("MenuReaderSettings",0);
		float multiplier = 0;
		
		try {
			multiplier = settings.getFloat("fontMultiplier", 1F);
		} catch (Exception e) {
			SharedPreferences.Editor editor = settings.edit();
	        editor.putFloat("fontMultiplier", 1F);
	        editor.commit();
		}
		
		return multiplier;
	}
	
	
}
