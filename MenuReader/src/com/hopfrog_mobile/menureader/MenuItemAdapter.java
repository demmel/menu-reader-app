package com.hopfrog_mobile.menureader;

import java.util.List;

import com.hopfrog_mobile.menureader.entities.MenuItem;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuItemAdapter extends ArrayAdapter<MenuItem> {
	
	public static final int NAME_SIZE = 18;
	public static final int COST_SIZE = 18;
	public static final int DESC_SIZE = 14;
	private float fontMultiplier;
	
	private static LayoutInflater inflater;
	private static SharedPreferences settings;
	
	public MenuItemAdapter(Context context, int textViewResourceId, List<MenuItem> objects) {
		super(context, textViewResourceId, objects);
		
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		settings = context.getSharedPreferences("MenuReaderSettings", 0);
		updateFontMultiplier();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("Redrawing all listview children with "+fontMultiplier);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.menu_item_list_item, null);
		}
		
		MenuItem item = this.getItem(position);
		
		TextView name = (TextView) convertView.findViewById(R.id.menu_item_list_name);
		TextView cost = (TextView) convertView.findViewById(R.id.menu_item_list_cost);
		TextView description = (TextView) convertView.findViewById(R.id.menu_item_list_description);
		ImageView veg = (ImageView) convertView.findViewById(R.id.menu_item_list_veg);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(COST_SIZE * fontMultiplier),(int)(COST_SIZE * fontMultiplier));
		veg.setLayoutParams(lp);

		
		if (item.isVeg()) {
			veg.setVisibility(View.VISIBLE);
		} else {
			veg.setVisibility(View.GONE);
		}
		
		name.setText(item.getName());
		name.setTextSize(NAME_SIZE * fontMultiplier);
		cost.setText(String.format("$%.2f", item.getCost()));
		cost.setTextSize(COST_SIZE * fontMultiplier);
		description.setText(item.getDescription());
		description.setTextSize(DESC_SIZE * fontMultiplier);
		
	    return convertView;
	  }
	
	public void updateFontMultiplier() {
		fontMultiplier = settings.getFloat("fontMultiplier", 1F);
	}
}
