package com.hopfrog_mobile.menureader.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtils {
	public static String sendGetRequest(String address, String resource, String params) throws IOException {
		return sendRequest(address, resource, params, "GET");
	}

	public static String sendPostRequest(String address, String resource, String params) throws IOException {
		return sendRequest(address, resource, params, "POST");
	}

	public static String sendPutRequest(String address, String resource, String params) throws IOException {
		return sendRequest(address, resource, params, "PUT");
	}

	public static String sendDeleteRequest(String address, String resource) throws IOException {
		return sendRequest(address, resource, "", "DELETE");
	}
	
	private static String sendRequest(String address, String resource, String params, String method) throws IOException {
		String urlString = address + "/" + resource;
		if(method.endsWith("GET")) urlString += "?" + params;
		URL url = new URL(urlString);
		HttpURLConnection connection;
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(method);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setUseCaches(false);

		if(method.equals("POST") || method.endsWith("PUT")) {
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
			
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(params);
			wr.flush();
			wr.close();
		}
		
		StringBuilder sb = new StringBuilder("");
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		while((line = rd.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		rd.close();

		connection.disconnect();
		return sb.toString();
	}
}
