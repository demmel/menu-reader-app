package com.hopfrog_mobile.menureader.backend;

import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.ClassNamesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

public class Driver {

	private static Logger log = LoggerFactory.getLogger(Driver.class);
	private static URI BASE_URI = getBaseURI();
	private static HttpServer httpServer;
	private static IDatabase db;

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://0.0.0.0/").port(8080).build();
	}

	public static void main(String[] args) {
		//Libraries use the jul logger and we're using slf4j to log4j.  We need to redirect library logs
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();

		log.info("Starting application");
		try {
			db = new MongoDatabase(MongoDatabase.DEFAULT_ADDR, MongoDatabase.DEFAULT_PORT);
		} catch (UnknownHostException e) {
			log.error("{}", e);
			System.exit(1);
		}
		
		ResourceConfig rc = new ClassNamesResourceConfig(RestaurantResource.class);
		rc.getSingletons().add(new SingletonTypeInjectableProvider<Context, IDatabase>(IDatabase.class, db){});
		
		try {
			httpServer = GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
		} catch (IllegalArgumentException e) {
			log.error("{}", e);
			System.exit(1);
		} catch (NullPointerException e) {
			log.error("{}", e);
			System.exit(1);
		} catch (IOException e) {
			log.error("{}", e);
			System.exit(1);
		}
		
		log.info("Application started - Hit any key to stop");
		try {
			System.in.read();
		} catch (IOException e) {
			log.error("{}", e);
			System.exit(1);
		}
		
		httpServer.stop();
	}
}
